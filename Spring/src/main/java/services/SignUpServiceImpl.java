package services;

import model.Account;
import validators.*;

public class SignUpServiceImpl implements SignUpService {

    private EmailValidate emailValidate;
    private PasswordValidate passwordValidateLength;
    private PasswordValidate passwordValidateCharacter;

    public SignUpServiceImpl(EmailValidate emailValidate, PasswordValidate passwordValidateLength, PasswordValidate passwordValidateCharacter) {
        this.emailValidate = emailValidate;
        this.passwordValidateLength = passwordValidateLength;
        this.passwordValidateCharacter = passwordValidateCharacter;
    }
    public void signUp(String email, String password) {

        emailValidate.validateEmail(email);
        passwordValidateLength.passValidate(password);
        passwordValidateCharacter.passValidate(password);
        Account account = new Account(1L, email, password);
        System.out.println("Аккаунт создан!");
        System.out.println(account);
    }
}

package validators;

public class PasswordValidateLengthImpl implements PasswordValidate {
    private int length;
    public PasswordValidateLengthImpl(int length) {
        this.length = length;
    }

    public void passValidate(String password) {
        if(password.length() < length){
            throw new IllegalArgumentException("Пароль должен быть равен или больше " + length + " символов");
        }
    }
}

package validators;

public interface EmailValidate {
    void validateEmail(String email);
}

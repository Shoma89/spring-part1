package validators;

public class EmailValidateRegexImpl implements EmailValidate {
    private char regex = '@';
    private int counter = 0;
    public void validateEmail(String email) {
        for (char charTemp:email.toCharArray()) {
            if(regex == charTemp){
                counter = counter + 1;
            }
            if (counter > 1){
                throw new IllegalArgumentException("Некорректная почта. '@' больше одной в почте быть не может!");
            }
        }
        if (counter == 0){
            throw new IllegalArgumentException("Некорректная почта. Нет '@'");
        }
    }
}

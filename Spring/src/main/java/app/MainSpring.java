package app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import services.SignUpService;
import services.SignUpServiceImpl;

public class MainSpring {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("context.xml");
        SignUpService signUpService =  applicationContext.getBean(SignUpServiceImpl.class);
        signUpService.signUp("super.mr-bean@yandex.ru", "!#&123123456789");
    }
}
